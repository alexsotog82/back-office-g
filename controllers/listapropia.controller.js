const ListaPropiaModel = require('../models/listaPropia');



exports.getListByUser = async(req,res) => {
    try {
        
        const list = await ListaPropiaModel.find({user: req.params.user})
                                            .populate('user', 'name')
                                            .populate('lista', '');

        if(!list) return res.status(400).json({
            success: false,
            msg: 'error en trae lista'
        })

        return res.status(200).json({
            success: true,
            count: list.length,
            data: list
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            msg: 'error en trae lista',
            error
        })
    }
}

exports.createListPropia = async(req,res) => {
    try {
       
        const list = await ListaPropiaModel.create(req.body)
       
        return res.status(200).json({
            success: true,
            data: list
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            msg: 'error en crear lista',
            error
        })
    }
}

exports.getAllListas = async(req,res) => {
    try {
       
        const list = await ListaPropiaModel.find()
                                           .populate('user', 'name')
                                           .populate('lista', '');
       
        return res.status(200).json({
            success: true,
            data: list
        })
    } catch (error) {
        return res.status(500).json({
            success: false,
            msg: 'error al traer todas las listas',
            error
        })
    }
}



