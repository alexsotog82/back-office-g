const path = require('path');
const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const cookieParser = require('cookie-parser');
const errorHandler = require('./middlewares/error');
const connectDB = require('./config/db');
const cors = require('cors');
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet');
const xss = require('xss-clean');
const hpp = require('hpp');
dotenv.config({ path: './config/config.env' });

connectDB();

const auth = require('./routes/auth');
const List = require('./routes/ListaVenta');
const docs = require('./routes/documentsRoutes');
const masterList = require('./routes/MasterList');
const newsRoutes = require('./routes/news');
const listaPropia = require('./routes/ListaPropia.route');

const app = express();

app.use(cors({origin: true, credentials: true}));
app.use(express.json());

app.use(cookieParser())

app.use(express.static(path.join(__dirname, 'public')));

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'))
}

app.use(mongoSanitize());

// Set security headers
app.use(helmet());

// Prevent XSS scripting  attacks
app.use(xss());

app.use(hpp());

// TODO: HACER UN MIDDLEWARE para subir archivos


// routes 
app.use('/api/v1/auth/', auth);
app.use('/api/v1/listSales', List);
app.use('/api/v1/docs', docs);
app.use('/api/v1/master', masterList);
app.use('/api/v1/news', newsRoutes);
app.use('/api/v1/propia', listaPropia)

app.get('/',(req,res) => {
    res.status(200).json({
        success: true,
        data: {
            message: 'bienvendido a las apis de gayol'
        }
    })
})

app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen( PORT,
                            console.log(`Serve runnig in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold));

process.on('unhandledRejection', (err, promise) => {
    console.log(`Error: ${err.message}`);
    server.close(() => process.exit(1));
})

