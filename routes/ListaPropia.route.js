const express = require('express');
const { createListPropia, getListByUser, getAllListas } = require('../controllers/listapropia.controller');


const router =  express.Router();


router.route('')
            .get(getAllListas)
            .post(createListPropia);

router.route('/:user')
        .get(getListByUser)


module.exports = router;