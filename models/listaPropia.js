const mongoose =  require('mongoose');


const listapropia = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        require: [true, 'se necesita de un usuario']
    },
    lista: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'listaVentas'
        }
    ,
    creatAt: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('propia', listapropia)